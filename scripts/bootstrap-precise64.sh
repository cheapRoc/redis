#!/usr/bin/env bash

#
# Bootstrap script for Vagrant's precise64 box in order to upgrade it's rather
# shitty Ruby and Chef installation which it packages with the box.
#
# I'm certain this would work with other boxes, with some minor configuration
# changes, but this script was written to hack around an issue with precise64
# generally.
#
# All we do is download and install Chef's x86_64 Debian/Ubuntu omnibus
# generated package.
#

# if `tty -s`; then
#    mesg n
# fi

export _pkg_tmp_dir="/tmp/chef-bootstrap"

[ -d $_pkg_tmp_dir ] || mkdir -p $_pkg_tmp_dir

export _chef_ver="11.6.2-1"
export _chef_os_name="ubuntu"
export _chef_os_ver="12.04"
export _chef_os_arch="x86_64"
export _omnibus_host="opscode-omnibus-packages.s3.amazonaws.com"

if [ $_chef_os_arch = "x86_64" ]; then
    export _chef_pkg_arch="amd64"
else
    export _chef_pkg_arch="i386"
fi

export _chef_pkg_name="chef_${_chef_ver}.${_chef_os_name}.${_chef_os_ver}_${_chef_pkg_arch}.deb"
export _chef_pkg_file="${_pkg_tmp_dir}/${_chef_pkg_name}"
export _chef_pkg_url="https://${_omnibus_host}/${_chef_os_name}/${_chef_os_ver}/${_chef_os_arch}/${_chef_pkg_name}"

apt-get update -qq
apt-get install -y curl

function pkg_download {
    echo "Downloading Chef package..."
    curl -sL $_chef_pkg_url -o $_chef_pkg_file
}

function pkg_install {
    dpkg -i $_chef_pkg_file
}

function pkg_status {
    curl -sL -XHEAD -w "%{http_code}" $_chef_pkg_url | sed 's/\s$//'
}

if [ -e $_chef_pkg_file ]; then
    echo
    echo "Chef package already downloaded and installed, enjoy!"
    echo
    exit 0
else
    export _pkg_http_code="$(pkg_status)"

    if [ $_pkg_http_code = "200" ]; then
        pkg_download
    else
        echo "Package http_code was ${_pkg_http_code}"
    fi
fi

if [ -e $_chef_pkg_file ]; then
    echo "Installing Chef package..."
    pkg_install
else
    echo "Package missing for some reason"
    exit 1
fi

echo
echo "Chef is now installed to /opt/chef, enjoy!"
echo
