#
# Locations
#

default['redis']['conf_dir']

default['redis']['conf_dir']          = "/etc/redis"
default['redis']['log_dir']           = "/var/log/redis"
default['redis']['data_dir']          = "/var/lib/redis"
default['redis']['bin_dir']           = "/usr/local/bin"
default['redis']['home_dir']          = "/usr/local/share/redis"
default['redis']['pid_file']          = "/var/run/redis.pid"

default['redis']['user']              = 'redis'
default['users']['redis']['uid']      = 335
default['groups']['redis']['gid']     = 335

#
# Install
#

default['redis']['version']           = "2.6.16"
default['redis']['release_url']       = "http://download.redis.io/releases/redis-#{node['redis']['version']}.tar.gz"

#
# Server
#

default['redis']['server']['addr']          = "0.0.0.0"
default['redis']['server']['port']          = 6379
default['redis']['server']['init']          = "upstart"
default['redis']['server']['daemonize']     = "yes"
default['redis']['server']['loglevel']      = "notice"
default['redis']['server']['logfile']       = "redis.log"
default['redis']['server']['databases']     = 16
default['redis']['server']['timeout']       = 300
default['redis']['server']['dbfilename']    = "dump.rdb"
default['redis']['server']['dir']           = node['redis']['data_dir']
default['redis']['server']['saves']         = [["900", "1"], ["300", "10"], ["60", "10000"]]
default['redis']['server']['tcp_keepalive'] = "0"

default['redis']['server']['stop_writes_on_bgsave_error'] = "yes"
default['redis']['server']['rdbcompression'] = "yes"
default['redis']['server']['rdbchecksum'] = "yes"

default['redis']['server']['slaveof'] = []
default['redis']['server']['masterauth'] = nil

default['redis']['server']['slave_serve_stale_data'] = 'yes'
default['redis']['server']['slave_read_only'] = 'yes'

default['redis']['server']['repl_ping_slave_period']   = nil
default['redis']['server']['repl_timeout']             = nil
default['redis']['server']['repl_disable_tcp_nodelay'] = "no"
default['redis']['server']['slave_priority'] = 100

default['redis']['server']['requirepass'] = nil
default['redis']['server']['maxclients']  = nil
default['redis']['server']['maxmemory']   = nil

default['redis']['server']['appendonly'] = "no"
default['redis']['server']['appendfilename'] = nil

# default[:redis][:slave]             = "no"
# if (node[:redis][:slave] == "yes")
#   # TODO: replace with discovery
#   default[:redis][:master_server]   = "redis-master." + domain
#   default[:redis][:master_port]     = "6379"
# end

# default[:redis][:shareobjects]      = "no"
# if (node[:redis][:shareobjects] == "yes")
#   default[:redis][:shareobjectspoolsize] = 1024
# end
